<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expert extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'experts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'image', 'telegram_id'];

    
}
