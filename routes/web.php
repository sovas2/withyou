<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Cache;

use Jenssegers\Agent\Agent;

use Illuminate\Http\Request;

use App\ZoomMeeting;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
x


Route::get('/', function (Request $request) {

	if (!$request->has('v1')){
		return redirect()->to('http://i.am.withyou.by');
	}

	$agent = new Agent();

	if ($agent->isMobile()){
		return view('welcome');
	}else{
    	//return redirect()->to('http://i.am.withyou.by');	
    	return view('welcome2');
	}
});


Route::get('/instant_zoom', function () {

    if (Cache::has(md5(Session::get('_token')))){
    	if (trim(Cache::get(md5(Session::get('_token')))) != '1'){
    		$redirectLink = Cache::get(md5(Session::get('_token')));
    		return redirect()->to($redirectLink);
    	}
    }else{
    	Cache::put(md5(Session::get('_token')), '1', 600);

	    try {
	        $response = \Telegram::getMe();

	        $botId = $response->getId();
	        $firstName = $response->getFirstName();
	        $username = $response->getUsername();

	        $response = Telegram::getUpdates();

	        $response = Telegram::sendMessage([
	          'chat_id' => -1001431772032, 
	          'text' => 'New Person Zoom Video ('.request()->ip().') in Channel o_O http://iam.withyou.by/respond_zoom/'.md5(Session::get('_token'))
	        ]);

	    } catch (Exception $e) {
	        
	    }
    }

    return '<meta http-equiv="refresh" content="3" /> Please wait a bit , we are finding ';
});

Route::get('/instant', function () {
    if (Cache::has(md5(Session::get('_token')))){
    	if (trim(Cache::get(md5(Session::get('_token')))) != '1'){
    		$redirectLink = Cache::get(md5(Session::get('_token')));
    		return redirect()->to($redirectLink);
    	}
    }else{
    	Cache::put(md5(Session::get('_token')), '1', 600);

	    try {
	        $response = \Telegram::getMe();

	        $botId = $response->getId();
	        $firstName = $response->getFirstName();
	        $username = $response->getUsername();

	        $response = Telegram::getUpdates();

	        $response = Telegram::sendMessage([
	          'chat_id' => -1001431772032, 
	          'text' => 'New Person ('.request()->ip().') in Channel o_O http://iam.withyou.by/respond/'.md5(Session::get('_token'))
	        ]);


	    } catch (Exception $e) {
	        
	    }
    }

    return '<meta http-equiv="refresh" content="3" /> Please wait a bit , we are finding ';
});

Route::get('/respond/{details}' , function($details){
	if (Cache::has($details) || Session::has($details) ){

		session([$details => '1']);

		if (strpos(request()->ip(), '149.154.161') > -1){
			return 'Hello Telegram :)';
		}

		if (Cache::has($details)){

			Cache::put($details, 'https://tlk.io/'. substr($details,0,8), 15);

		    try {
		        $response = \Telegram::getMe();

		        $botId = $response->getId();
		        $firstName = $response->getFirstName();
		        $username = $response->getUsername();

		        $response = Telegram::getUpdates();

		        $response = Telegram::sendMessage([
		          'chat_id' => -1001431772032, 
		          'text' => 'Session '.substr($details,0,12).' has been assigned to Specialist '.request()->ip()
		        ]);


		    } catch (Exception $e) {
		        
		    }
		}

		return redirect()->to('https://tlk.io/'. substr($details,0,8));		
	}else{
		return 'session already tooken';
	}
});


Route::get('/respond_zoom/{details}' , function($details){
	if (Cache::has($details) || Session::has($details) ){

		if (strpos(request()->ip(), '149.154.161') > -1){
			return 'Hello Telegram :)';
		}

		if (Cache::has($details)){

			$zoom_meeting = new ZoomMeeting();
			try{
				$z = $zoom_meeting->createAMeeting(
					array(
						'start_date'=>date("Y-m-d h:i:s", strtotime('now')),
						'meetingTopic'=>'I am With You Therapy Session'
					)
				);

				Cache::put($details, $z->join_url , 15);

				session([$details => $z->join_url]);

			    try {
			        $response = \Telegram::getMe();

			        $botId = $response->getId();
			        $firstName = $response->getFirstName();
			        $username = $response->getUsername();

			        $response = Telegram::getUpdates();

			        $response = Telegram::sendMessage([
			          'chat_id' => -1001431772032, 
			          'text' => 'Session ZoomChat'.substr($details,0,12).' has been assigned to Specialist '.request()->ip()
			        ]);

			        return redirect()->to($z->join_url);	

			    } catch (Exception $e) {
			    	return redirect()->to('https://iam.withyou.by/instant');   
			    }

			} catch (Exception $ex) {
				return redirect()->to('https://iam.withyou.by/instant');
			}
		}else{
			return redirect()->to(session($details));	
		}	
	}else{
		return 'session already tooken';
	}
});


Route::resource('moderator/expert', 'Moderator\\ExpertController');